public class LoopsTasks {


  /**
   * Returns the 'Fizz','Buzz' or an original number as String using the following rules:
   * 1) return original number as String
   * 2) but if number multiples of three return 'Fizz'
   * 3) for the multiples of five return 'Buzz'
   * 4) for numbers which are multiples of both three and five return 'FizzBuzz'
   *
   * @param {number} num
   * @return {any}
   *
   * @example
   *   2 =>  '2'
   *   3 => 'Fizz'
   *   5 => 'Buzz'
   *   4 => '4'
   *  15 => 'FizzBuzz'
   *  20 => 'Buzz'
   *  21 => 'Fizz'
   *
   */
    public static String getFizzBuzz(Integer num) {
        String f = 'Fizz';
        String b = 'Buzz';
        String fb = 'FizzBuzz';
        String str = String.valueOf(num);
        if (Math.mod(num, 3) == 0 && Math.mod(num, 5) == 0) {
            return fb;
        } else {
            if (Math.mod(num, 5) == 0) {
                return b;
            } else {
                if (Math.mod(num, 3) == 0) {
                    return f;
                } else {
                    return str;
                }
            }
        }
    }
    /**
     * Returns the factorial of the specified integer n.
     *
     * @param {number} n
     * @return {number}
     *
     * @example:
     *   1  => 1
     *   5  => 120
     *   10 => 3628800
     */
  public static Integer getFactorial(Integer num) {
    Integer value = 1;
    if (num < 0) {
      System.debug('No negative numbers');
    }
    else {
      for (Integer i = 1; i <= num; i++) {
        value *= i;
      }
    }
    return value;
  }

  /**
   * Returns the sum of integer numbers between n1 and n2 (inclusive).
   *
   * @param {number} n1
   * @param {number} n2
   * @return {number}
   *
   * @example:
   *   1,2   =>  3  ( = 1+2 )
   *   5,10  =>  45 ( = 5+6+7+8+9+10 )
   *   -1,1  =>  0  ( = -1 + 0 + 1 )
   */
  public static Integer getSumBetweenNumbers(Integer num1, Integer num2) {
    Integer value = 0;
      for (Integer i = num1; i <= num2; i++) {
        value += i;
      }
    return value;
  }

  /**
   * Returns true, if a triangle can be built with the specified sides a,b,c and false in any other ways.
   *
   * @param {number} a
   * @param {number} b
   * @param {number} c
   * @return {bool}
   *
   * @example:
   *   1,2,3    =>  false
   *   3,4,5    =>  true
   *   10,1,1   =>  false
   *   10,10,10 =>  true
   */
  public static Boolean isTriangle(Integer a, Integer b, Integer c) {
    Boolean value = false;
    if ((a<b+c) && (b<a+c) && (c<a+b)) {
      value = true;
    }
    return value;
  }

  /**
   * Returns true, if two specified axis-aligned rectangles overlap, otherwise false.
   * Each rectangle representing by Map<String, Integer> 
   *  {
   *     top: 5,
   *     left: 5,
   *     width: 20,
   *     height: 10
   *  }
   * 
   *  (5;5)
   *     -------------  
   *     |           | 
   *     |           |  height = 10
   *     ------------- 
   *        width=20    
   * 
   * NOTE: Please use canvas coordinate space (https://developer.mozilla.org/en-US/docs/Web/API/Canvas_API/Tutorial/Drawing_shapes#The_grid),
   * it differs from Cartesian coordinate system.
   * 
   * @param {Map<String, Integer>} rect1
   * @param {Map<String, Integer>} rect2
   * @return {bool}
   *
   * @example:
   *   { top: 0, left: 0, width: 10, height: 10 },
   *   { top: 5, left: 5, width: 20, height: 20 }    =>  true
   * 
   *   { top: 0, left: 0, width: 10, height: 10 },
   *   { top:20, left:20, width: 20, height: 20 }    =>  false
   *  
   */
    public static Boolean doRectanglesOverlap(Map<String, Integer> rect1, Map<String, Integer> rect2) {
      Boolean bool = true;
      if (rect1.get('top') > rect2.get('width') + rect2.get('top') || rect2.get('top') > rect1.get('width') + rect1.get('top')) {
          return !bool;
      } else
              if (rect1.get('left') > rect2.get('height') + rect2.get('left')|| rect2.get('left') > rect1.get('height') +rect1.get('left')) {
                  return !bool;
              } else {
                  return bool;
              }
  }

  /**
   * Returns true, if point lies inside the circle, otherwise false.
   * Circle is:
   *     Center Map<String, Double>: {
   *       x: 5,       
   *       y: 5
   *     },        
   *     Radius Integer: 20
   * Point is Map<String, Double> 
   *  {
   *     x: 5,
   *     y: 5
   *  }
   * 
   * @param {Map<String, Double>} center
   * @param {Integer} raduis
   * @param {Map<String, Double>} point
   * @return {bool}
   *
   * @example:
   *   center: { x:0, y:0 }, radius: 10, point: { x:0, y:0 }     => true
   *   center: { x:0, y:0 }, radius:10,  point: { x:10, y:10 }   => false
   *   
   */
  public static Boolean isInsideCircle(Map<String, Double> center, Integer radius, Map<String, Double> point) {
      Boolean bool = true;
      if ((point.get('x') - center.get('x')) * (point.get('x') - center.get('x')) + ((point.get('y') - center.get('y')) * (point.get('y') - center.get('y'))) < radius * radius) {
          return bool;
      } else {
          return !bool;
      }
  }

  /**
   * Returns the first non repeated char in the specified strings otherwise returns null.
   *
   * @param {string} str
   * @return {string}
   *
   * @example:
   *   'The quick brown fox jumps over the lazy dog' => 'T'
   *   'abracadabra'  => 'c'
   *   'entente' => null
   *
   */
  public static String findFirstSingleChar(String str) {
      Set<String> nonDup = new Set<String>(), dup = new Set<String>();
      for(String value : str.split('')) {
          if (!String.isBlank(value)) {
              if (nonDup.contains(value)) {
                  dup.add(value);
              }
              nonDup.add(value);
          }
      }
      nonDup.removeAll(dup);
      String searchString = String.join(new List<String>(dup),'');
      if (nonDup.isEmpty()) {
          return null;
      } else {
          return str.substring(str.indexOfAnyBut(searchString), str.indexOfAnyBut(searchString)+1);
      }
  }

  /**
   * Returns the string representation of math interval, specified by two points and include / exclude flags.
   * See the details: https://en.wikipedia.org/wiki/Interval_(mathematics)
   *
   * Please take attention, that the smaller number should be the first in the notation
   *
   * @param {number} a
   * @param {number} b
   * @param {bool} isStartIncluded
   * @param {bool} isEndIncluded
   * @return {string}
   *
   * @example
   *   0, 1, true, true   => '[0, 1]'
   *   0, 1, true, false  => '[0, 1)'
   *   0, 1, false, true  => '(0, 1]'
   *   0, 1, false, false => '(0, 1)'
   * Smaller number has to be first :
   *   5, 3, true, true   => '[3, 5]'
   *
   */
  public static String getIntervalString(Integer a, Integer b, Boolean isStartIncluded, Boolean isEndIncluded) {
      string value;
      String bracket1;
      String bracket2;
      if (isStartIncluded == true) {
          bracket1 = '[';
      } else {
          bracket1 = '(';
      }
      if (isEndIncluded == true) {
          bracket2 = ']';
      } else {
          bracket2 = ')';
      }
      value = bracket1 + a + ', ' + b + bracket2;
      if (a > b) {
          value = bracket1 + b + ', ' + a + bracket2;
      }
      return value;
  }

  /**
   * Reverse the specified string (put all chars in reverse order)
   *
   * @param {string} str
   * @return {string}
   *
   * @example:
   * 'The quick brown fox jumps over the lazy dog' => 'god yzal eht revo spmuj xof nworb kciuq ehT'
   * 'abracadabra' => 'arbadacarba'
   * 'rotator' => 'rotator'
   * 'noon' => 'noon'
   */
  public static String reverseString(String str) {
    return str.reverse();
  }

  /**
   * Reverse the specified integer number (put all digits in reverse order)
   *
   * @param {number} num
   * @return {number}
   *
   * @example:
   *   12345 => 54321
   *   1111  => 1111
   *   87354 => 45378
   *   34143 => 34143
   */
  public static Integer reverseInteger(Integer num) {
      String str = String.valueOf(num);
      String str2 = str.reverse();
      Integer result = Integer.valueOf(str2);
      return result;
  }

  /**
   * Returns the digital root of integer:
   *   step1 : find sum of all digits
   *   step2 : if sum > 9 then goto step1 otherwise return the sum
   *
   * @param {number} n
   * @return {number}
   *
   * @example:
   *   12345 ( 1+2+3+4+5 = 15, 1+5 = 6) => 6
   *   23456 ( 2+3+4+5+6 = 20, 2+0 = 2) => 2
   *   10000 ( 1+0+0+0+0 = 1 ) => 1
   *   165536 (1+6+5+5+3+6 = 26,  2+6 = 8) => 8
   */
  public static Integer getDigitalRoot(Integer num) {
      Integer sum = (1 + math.mod((num - 1 ), 9));
      return sum;
  }

  /**
   * Returns true if the specified string has the balanced brackets and false otherwise.
   * Balanced means that is, whether it consists entirely of pairs of opening/closing brackets
   * (in that order), none of which mis-nest.
   * Brackets include [],(),{},<>
   *
   * @param {string} str
   * @return {boolean}
   *
   * @example:
   *   '' => true
   *   '[]'  => true
   *   '{}'  => true
   *   '()   => true
   *   '[[]' => false
   *   ']['  => false
   *   '[[][][[]]]' => true
   *   '[[][]][' => false
   *   '{)' = false
   *   '{[(<{[]}>)]}' = true 
   */
  public static Boolean isBracketsBalanced(String str) {
      Boolean isBalanced = false;
      List<String> list1 = new List<String>{
              '(', '{', '<', '['
      };
      List<String> list2 = new List<String>{
              ')', '}', '>', ']'
      };
      List<String> brack = new List<String>();
      List<String> chars = str.split('');
      for (String ch : chars) {
          if (list1.contains(ch)) {
              brack.add(ch);
          }
          if (list2.contains(ch)) {
              if (brack.size() > 0) {
                  if (brack[brack.size() - 1] == list1[list2.indexOf(ch)]) {
                      brack.remove(brack.size() - 1);
                  } else {
                      brack.add('error');
                      break;
                  }
              } else {
                  brack.add('error');
                  break;
              }
          }
      }

      if (brack.size() == 0)
          isBalanced = true;
      return isBalanced;
  }


  /**
  *   List<String> dup = new List<String>();
      List<String> nonDup = new List<String>();
      for(String value:str.split('')) {
          if(!String.isBlank(value)) {
              if(nonDup.contains(value)) {
                  dup.add(value);
              }
              nonDup.add(value);
          }
      }
      for(String val2: nonDup){
          if(dup.contains(val2)){
             dup.remove(dup.indexOf(val2));
          }
      }

      if (!dup.isEmpty()) {
          return false;
      } else {
          return true;
      }
  }
   * Returns the human readable string of time period specified by the start and end time.
   * The result string should be constrcuted using the folliwing rules:
   *
   * ---------------------------------------------------------------------
   *   Difference                 |  Result
   * ---------------------------------------------------------------------
   *    0 to 45 seconds           |  a few seconds ago
   *   45 to 90 seconds           |  a minute ago
   *   90 seconds to 45 minutes   |  2 minutes ago ... 45 minutes ago
   *   45 to 90 minutes           |  an hour ago
   *  90 minutes to 22 hours      |  2 hours ago ... 22 hours ago
   *  22 to 36 hours              |  a day ago
   *  36 hours to 25 days         |  2 days ago ... 25 days ago
   *  25 to 45 days               |  a month ago
   *  45 to 345 days              |  2 months ago ... 11 months ago
   *  345 to 545 days (1.5 years) |  a year ago
   *  546 days+                   |  2 years ago ... 20 years ago
   * ---------------------------------------------------------------------
   *
   * @param {DateTime} startDate
   * @param {DateTime} endDate
   * @return {string}
   *
   * @example
   *   DateTime('2000-01-01 01:00:00'), DateTime('2000-01-01 01:00:01')  => 'a few seconds ago'
   *   DateTime('2000-01-01 01:00:00'), DateTime('2000-01-01 01:00:05')  => '5 minutes ago'
   *   DateTime('2000-01-01 01:00:00'), DateTime('2000-01-02 03:00:05')  => 'a day ago'
   *   DateTime('2000-01-01 01:00:00'), DateTime('2015-01-02 03:00:05')  => '15 years ago'
   *
   */
  public static String timespanToHumanString(DateTime startDate, DateTime endDate) {
      Decimal sec = (endDate.getTime() - startDate.getTime()) / 1000;
      if (0 < sec && sec <= 45) {
          return 'a few seconds ago';
      } else
              if (45 < sec && sec <= 90) {
                  return 'a minute ago';

              } else
                      if (90 < sec && sec <= 45 * 60) {
                          return String.valueOf(math.round(sec / 60 - 0.01)) + ' minutes ago';

                      } else
                              if (45 * 60 < sec && sec <= 90 * 60) {
                                  return 'an hour ago';

                              } else
                                      if (90 * 60 < sec && sec <= 22 * 60 * 60) {
                                          return String.valueOf(math.round(sec / 60 / 60)) + ' hours ago';

                                      } else
                                              if (22 * 60 * 60 < sec && sec <= 36 * 60 * 60) {
                                                  return 'a day ago';

                                              } else
                                                      if (36 * 60 * 60 < sec && sec <= 25 * 24 * 60 * 60) {
                                                          return String.valueOf(math.round(sec / 60 / 60 / 24)) + ' days ago';

                                                      } else
                                                              if ( 25*24*60*60 < sec && sec <= 45*24*60*60){
                                                                  return 'a month ago';

                                                              } else
                                                                      if (45*24*60*60 <sec && sec <= 345*24*60*60){
                                                                          return String.valueOf(math.round(sec/60/60/24/30))+ ' months ago';

                                                                      } else
                                                                              if ( 345*24*60*60 < sec && sec <= 545*24*60*60){
                                                                                  return 'a year ago';
                                                                              }
      return String.valueOf(math.round(sec/60/60/24/30/12)) + ' years ago';
  }
}