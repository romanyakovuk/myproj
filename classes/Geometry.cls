public class Geometry {
    public Integer side1;
    public Integer side2;
    public Integer side3;
    public Geometry (Integer side3){
        this.side3 = side3;
    }
    public Geometry (Integer side1, Integer side2){
        this.side1 = side1;
        this.side2 = side2;
    }
    private Boolean check(){
        Boolean value = false;
        if (this.side1 == NULL || this.side1 <= 0 || this.side2 == NULL || this.side2 <= 0) {
            System.debug('Wrong Value');
        }
        else {value = true;}
        return value;
    }
    public Integer perimetr() {
        Integer value;
        if (check()) {
            value = this.side1 * this.side2;
        }
        return value;
    }
    public Integer area() {
        Integer value;
        if (check()) {
            value = (this.side1 + this.side2) * 2;
        }
        return value;
    }
    public Decimal sqrt1() {
        Decimal value;
        if (check()) {
            value = (this.side1 * this.side1) + (this.side2 * this.side2);
        }
        return math.SQRT(value);}
    }








/*
Geometry geo = New Geometry();
for (Integer i=0; i<9; i++){
    System.debug(i + '===>>> ' + geo.area(-1,-1));
    System.debug('===>>> ' + geo.perimetr(1,4));
    System.debug('===>>> ' + geo.sqrt1(1,5));}
}


    public String Determinant(Integer side1, Integer side2) {
        String value;
        if (side1 == side2) {
            value = 'Its a SQUARE!!!';
        } else {
            value = 'Its a RECTANGLE!!!';
        }
        return value;
    }
        public Integer Parametr(Integer side1, Integer side2){
        this.side1 = side1;
        this.side2 = side2;
    }


______________________________________________________________________________________________________________
    List<Contact> allcontacts = new List<Contact>();
List<Integer> succsesslst = new List<Integer>();
for (Integer i = 0; i < 10; i++){
    Contact newContact = new Contact(FirstName ='Firstname', LastName = 'Lastname');
    allcontacts.add(newContact);
}
for (Integer i = 0; i < 10; i++){
    Contact newContact = new Contact(FirstName ='Firstname', LastName ='');
    allcontacts.add(newContact);
}
Savepoint sp = Database.setSavepoint();
Database.SaveResult[] srlst = Database.insert(allcontacts, false);
Integer i = 0;
for (Database.SaveResult sr : srlst) {
    if (sr.isSuccess()) {
        succsesslst.add(i);
        i++;
    }
}
System.debug('====>>>' + succsesslst);
if (succsesslst.size() < allcontacts.size()){
    Database.rollback(sp);
}
 */