public class ProductTablePageController{
    public List<ProductTable__c> productList {get;set;}
    public BOOLEAN addDisplay {get;set;}
    public ProductTable__c insertForm {get;set;}
    public List<Wrapperclass> wrapperlst {get; set;}
    List<ProductTable__c> stulist=new List<ProductTable__c>();
    public Boolean SaveMethod {get; set;}
    public ProductTablePageController() {
        getProducts();
        insertForm=new ProductTable__c();
        SaveMethod=false;
        wrapperlst = new List<Wrapperclass>();
        for(ProductTable__c st: [select AddedDate__c,Amount__c,Price__c,ProductType__c,Name,Available__c,ReleaseDate__c 
                                 from ProductTable__c])
            wrapperlst.add(new wrapperclass(st));
        
    }
    public void getProducts() {
        productList=[
            SELECT AddedDate__c,Amount__c,Price__c,ProductType__c,Name,Available__c,ReleaseDate__c  
            FROM ProductTable__c 
            ORDER BY AddedDate__c DESC];
    }
    public void closeDisplay() {
        addDisplay=false;
    }
    public void showDisplay() {
        addDisplay=true;
    }
    public void insertNewProductTable() {
        insert insertForm;
        insertForm=new ProductTable__c();
        closeDisplay();
        getProducts();
    }
    
    public void deleteProductTable(){
        delete stulist;
    }
    
    public void updateNewProductTable(){
        
        if(wrapperlst.size() >0){
            integer count=0;
            
            
            for(Wrapperclass wrap : wrapperlst){
                if(wrap.isSelEdit == true)
                    count++;
                else if(wrap.isSelDelete == true)
                    stulist.add(wrap.stu);
            }
            if(count>0)
                savemethod=true;
            if(stulist.size()>0){
                deleteProductTable();
                wrapperlst.clear();
                for(ProductTable__c st: [Select AddedDate__c,Amount__c,Price__c,ProductType__c,Name,Available__c,ReleaseDate__c 
                                         from ProductTable__c])
                    wrapperlst.add(new Wrapperclass(st));
            }
            
        }
        
    }
    
    
    public void Save(){
        list<ProductTable__c> stulist=new list<ProductTable__c>();
        for(wrapperclass wrap : wrapperlst){
            wrap.isseledit = false;
            stulist.add(wrap.stu); 
        }
        SaveMethod=false;
        if(stulist.size()>0)
            update stuList;
        
    }
    public class wrapperclass{
        public ProductTable__c stu{get;set;}
        public boolean IsSelEdit{get;set;}
        public boolean IsSelDelete{get;set;}
        public wrapperclass(ProductTable__c st){
            stu = st;
            isSelDelete = false;
            IsSelEdit = false;
        }
    }
    String keyword;
    List<ProductTable__c> results;
    public String getkeyword(){
        return keyword;
    }
    public List<ProductTable__c> getresults(){
        return results;
    }
    public void setkeyword(String input){
        keyword = input;
    }
    public PageReference search_now(){
        results = (List<ProductTable__c>)[FIND :keyword IN ALL FIELDS RETURNING ProductTable__c(Name, AddedDate__c, Amount__c, Price__c, ProductType__c, Available__c, ReleaseDate__c)][0];
        return null;
    }  
}