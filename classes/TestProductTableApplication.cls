@IsTest
public class TestProductTableApplication {
    @testSetup
    static void setup(){
        ProductTable__c testObject = new ProductTable__c(
            Name = 'random1',
            Amount__c = 1);
        insert testObject;
        
    }
    @isTest
    static void testGetProduct(){
        Test.startTest();
        ProductTablePageController controller = new ProductTablePageController();
        controller.getProducts();
        Test.stopTest();
        System.debug(controller.productList);
        System.assert(true);
        
    }
    @isTest
    static void testCloseDisplay(){
        ProductTablePageController controller = new ProductTablePageController();
        controller.closeDisplay();
        
        System.assert(true);
    }
    @isTest
    static void testShowDisplay(){
        ProductTablePageController controller = new ProductTablePageController();
        controller.showDisplay();
        
        System.assert(true);
    }
    
    @isTest
    static void testinsertNewProductTable(){
        ProductTablePageController controller = new ProductTablePageController();
        controller.insertNewProductTable();
        System.assert(true);
    }
     @isTest
    static void testeditDelaction(){
        ProductTablePageController controller = new ProductTablePageController();
        controller.updateNewProductTable();
        System.assert(true);
    }
     @isTest
    static void testSave(){
        ProductTablePageController controller = new ProductTablePageController();
        controller.Save();
        System.assert(true);
    }
    @isTest
    static void testGatekeyword(){
        ProductTablePageController controller = new ProductTablePageController();
        controller.Getkeyword();
        System.assert(true);
    }
}