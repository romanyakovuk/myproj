public interface TEst2 {
    Integer TEst2(Integer a, Integer b);
}
/*
SECTION 4.1

List<Contact> contacts = new List<Contact>();
for (integer i = 0; i < 5; i ++){
    Contact c = new Contact(FirstName= 'FirstName', LastName = 'LastName');
    contacts.add(c);
}
insert contacts;
________________________________________________________________
List<Contact> contacts = [SELECT FirstName, LastName, Title FROM Contact WHERE LastName = 'LastName'];
for (integer i = 0; i < 5; i ++){
    Contact c = new Contact(FirstName= 'FirstName', LastName = 'LastName', Title = 'Developer');
    contacts.add(c);
}
for (integer i = 0; i < contacts.size(); i ++){
    Contacts[i].Title = 'Developer';
}
upsert contacts;
________________________________________________________________
List<Contact> contacts = [SELECT FirstName, LastName, Title FROM Contact WHERE LastName = 'LastName'];
delete contacts;
______________________________________________________________
List<Contact> contacts = [SELECT FirstName, LastName, Title FROM Contact WHERE Title = 'Developer' ALL ROWS];
undelete contacts;
______________________________________________________________
List<Contact> allcontacts = new List<Contact>();
List<Integer> succsesslst = new List<Integer>();
for (Integer i = 0; i < 10; i++){
Contact newContact = new Contact(FirstName ='Firstname', LastName = 'Lastname');
allcontacts.add(newContact);
}
for (Integer i = 0; i < 10; i++){
Contact newContact = new Contact(FirstName ='Firstname', LastName ='');
allcontacts.add(newContact);
}
Savepoint sp = Database.setSavepoint();
Database.SaveResult[] srlst = Database.insert(allcontacts, false);
Integer i = 0;
for (Database.SaveResult sr : srlst) {
if (sr.isSuccess()) {
succsesslst.add(i);
i++;
}
}
System.debug('====>>>' + succsesslst);
if (succsesslst.size() < allcontacts.size()){
Database.rollback(sp);
________________________________________________________________________________
SECTION 4.2

[SELECT Id, Name, Account.Name FROM Contact WHERE LastName = 'Bond'];
_______________________________________________________________________
[SELECT Id, Name, Account.Name FROM Contact WHERE LastName = 'D’'];
_______________________________________________________________________
[SELECT Id, Name, Account.Name FROM Contact ORDER BY LastName ASC];
_______________________________________________________________________
[SELECT Id, Name, Account.Name FROM Contact LIMIT 12 OFFSET 3];
_______________________________________________________________________
[SELECT Id, Name, Account.Name FROM Contact WHERE Firstname = 'Tim' AND Lastname = 'Forbes'];
_______________________________________________________________________
[SELECT MAX(Amount) FROM Opportunity];
_______________________________________________________________________
[SELECT Name, Amount FROM Opportunity WHERE Amount>500000];
_______________________________________________________________________
[SELECT Id, Name, NumberOfEmployees FROM Lead WHERE NumberOfEmployees>0];

*/