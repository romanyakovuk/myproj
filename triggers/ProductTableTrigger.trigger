trigger ProductTableTrigger on ProductTable__c (before insert, before update) {
    for(ProductTable__c item : Trigger.new){
        if(Trigger.isInsert && item.AddedDate__c == NULL){
            item.AddedDate__c = ProductTableTriggerHelper.getDate();}
        if(item.Amount__c >= 0){
            item.Available__c = true;}
    }
}